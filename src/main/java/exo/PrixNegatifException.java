package exo;

public class PrixNegatifException extends Exception {
	
	private double prixHT;
	
	public PrixNegatifException(Double prixHT) {
		super("Prix négatif trouvé : " + prixHT);
		this.prixHT = prixHT;
	}

	public Object getPrixHT() {
		// TODO Auto-generated method stub
		return prixHT;
	}
	
}
