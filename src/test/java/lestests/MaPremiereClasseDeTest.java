package lestests;

import org.junit.Assert;
import org.junit.Test;

public class MaPremiereClasseDeTest {
	
	@Test
	public void abs_QuandNombrePositif_AlorsLeResultatEstLeNombre() {
		// Arrange 
		double leNombre = 1234;
		
		//Act
		double resultat = Math.abs(leNombre);
		
		//Assert
		Assert.assertEquals(leNombre, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreNegatif_AlorsLeResultatEstLaValeurAbsolue() {
		// Arrange 
		double leNombre = -1234;
		
		//Act
		double resultat = Math.abs(leNombre);
		
		//Assert
		Assert.assertEquals(1234, resultat, 0);
	}
	
	@Test
	public void abs_QuandNombreZero_AlorsLeResultatEstZero() {
		// Arrange 
		double leNombre = 0;
		
		//Act
		double resultat = Math.abs(leNombre);
		
		//Assert
		Assert.assertEquals(0, resultat, 0);
	}
}
